# DownloadFullsize
![DownloadFullsize](./screenshot.jpg) 

## Description
DownloadFullsize is a plugin for [Gallery 3](http://gallery.menalto.com/) which will add a link to the photo and movie view pages that will allow visitors to download the "full sized" version of the current photo or movie.  This link will only be displayed if the current user has sufficient privileges to access the full sized photos and movies, so admin's who have configured their galleries to not allow anonymous guests to access the full sized image or who only allow full sized access in specific albums are still able to install this plugin.  

Additional information can be found on the [Gallery 3 Forums](http://gallery.menalto.com/node/103278).

**License:** [GPL v.2](http://www.gnu.org/licenses/old-licenses/gpl-2.0.html)

## Instructions
This installs like any other Gallery 3 module.  Download and extract it into your modules folder.  Afterwards log into the Gallery web interface and enable in under the Admin -> Modules menu.

Once installed, this module will create a sidebar block with a "Download Fullsize Image" button that can be displayed on Photo pages.  Alternately there is a Admin > Settings > Download Photo Links page where a "Show Floppy Disk Picture Link" setting can be used to display a download icon at the top of the sidebar.  If the KeepOriginal module is installed an additional option will be available to optionally configure this module to download original images instead of fullsize images.

## History
**Version 1.6.0:**
> - Gallery 3.1.3 compatibility fixes.
> - Released 28 August 2021.
>
> Download: [Version 1.6.0](/uploads/756a3b8037ce726f90353ca6eb8eff1f/downloadfullsize160.zip)

**Version 1.5.1:**
> - Updated module.info file for Gallery 3.0.2 compatibility.
> - Released 15 August 2011.
>
> Download: [Version 1.5.1](/uploads/3c9898c768a1123886497951aa9282c7/downloadfullsize151.zip)

**Version 1.5.0:**
> - Updated for recent changes to Gallery 3's module API.
> - Removed checkbox for enabling / disabling the sidebar download button.
> - Reworked sidebar code to use Gallery's new dynamic sidebar feature.
> - Released 30 December 2009.
>
> Download: [Version 1.5.0](/uploads/4274970ef4efeccff0c466b6eab783c9/downloadfullsize150.zip)

**Version 1.4.0:**
> - Merged in ckieffer's CSS changes for Gallery 3 Git
> - Re-worked how the CSS code is being inserted (which should make it easier for theme developers to override it).
> - Tested everything against current git (as of commit b6c1ba7ea6416630b2a44b3df8400a2d48460b0a)
> - Released 12 October 2009
>
> Download: [Version 1.4.0](/uploads/adccb4224df0c2eea64c8e34a8396046/downloadfullsize140.zip)

**Version 1.3.0:**
> - Tested against Gallery 3, Beta 3.
> - I've re-worked some of the code to no longer require modifying the default theme.
> - In the event that my KeepOriginal module is also installed, an additional admin option will appear to allow visitors to download the original image instead of the rotated image, if available.
> - If you're using the "sidebar text" download link, a download link will appear on video pages. I haven't been able to figure out how to do the same for the "floppy disk" download link yet.
> - Released 16 September 2009.
>
> Download: [Version 1.3.0](/uploads/aa171da918e624e69f0ddb99b643d6d3/downloadfullsize130.zip)

**Version 1.2.0:**
> - Includes bharat's changes for the recent API update.
> - Released 31 July 2009.
>
> Download: [Version 1.2.0](/uploads/20fa3357fcb2586a48c43494aa7c297d/downloadfullsize120.zip)

**Version 1.1.2:**
> - Imported cowlemon's changes to match the default Gallery 3 theme.
> - Released 08 July 2009.
>
> Download: [Version 1.1.2](/uploads/63d2bf82ff027051a7232b132fd92fec/downloadfullsize112.zip)

**Version 1.1.1:**
> - Imported bharat's changes for this module.
> - Released 21 June 2009
>
> Download: [Version 1.1.1](/uploads/f2e6811b897f03d7e78ed715b3d5edce/downloadfullsize111.zip)

**Version 1.1.0:**
> - Added a "Floppy Disk" image that visitors can click on to download the photo.
> - Added an administrative screen that the site Admin can use to specify if the Floppy Image or Text Link should be displayed.
> - Reworked the download code to cause a "Save As" dialog to appear instead of just direct-linking to the photo.
> - Released on 20 June 2009
>
> Download: [Version 1.1.0](/uploads/7de78f084f015e4ba60365a9a44a7cda/downloadfullsize110.zip)

**Version 1.0.0:**
> - Initial Release
> - Released on 19 June 2009
>
> Download: [Version 1.0.0](/uploads/391deac599f617ebeff82200db5bea84/downloadfullsize100.zip)

